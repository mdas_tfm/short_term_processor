#!/bin/bash

set -euo pipefail
set -x

KUBERNETES_ASSETS="${CI_PROJECT_DIR}/build/kubernetes"
LOCAL_ASSETS="${CI_PROJECT_DIR}/../production/assets/${CI_PROJECT_NAME}"

print_info() {
    printf "\e[36m[INFO] ==> %s \e[0m\n" "${1:-'unknown'}"
}

print_ok() {
    printf "\e[32m[SUCCESS] ==> %s \e[0m\n" "${1:-'unknown'}"
}

print_error() {
    printf "==> \e[31m[ERROR] ==> failed %s \e[0m\n" "${1:-'unknown'}"
    exit 1
}

if [ ! -d "${KUBERNETES_ASSETS}" ]; then
    print_error "Could not find Kubernetes assets in ${KUBERNETES_ASSETS}"
fi

if [ ! -f "${KUBERNETES_ASSETS}/kubernetes.json" ]; then
    print_error "Could not find Kubernetes JSON assets in ${KUBERNETES_ASSETS}"
fi

if [ ! -d "${LOCAL_ASSETS}" ]; then
    mkdir -p "${LOCAL_ASSETS}"
fi

cp "${KUBERNETES_ASSETS}"/*.json "${LOCAL_ASSETS}/" || print_error "failed copying assets"

print_ok "Assets for ${CI_PROJECT_NAME} successfully copied to ${LOCAL_ASSETS}"
exit 0
