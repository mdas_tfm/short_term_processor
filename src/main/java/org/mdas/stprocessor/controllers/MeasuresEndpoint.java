package org.mdas.stprocessor.controllers;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.microprofile.metrics.MetricUnits;
import org.eclipse.microprofile.metrics.annotation.Counted;
import org.eclipse.microprofile.metrics.annotation.Timed;
import org.mdas.stprocessor.model.MeasureWithAggregatedData;
import org.mdas.stprocessor.streams.GetMeasurementDataResult;
import org.mdas.stprocessor.streams.InteractiveQueries;
import org.mdas.stprocessor.streams.PipelineMetadata;

@ApplicationScoped
@Path("/measures")
public class MeasuresEndpoint {

    @Inject
    InteractiveQueries interactiveQueries;

    @GET
    @Path("/{sensorId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Counted(name = "performedChecks", description = "How many checks have been performed.", reusable = true)
    @Timed(name = "checksTimer", description = "A measure of how long it takes to perform the " +
            "operation.", unit = MetricUnits.MILLISECONDS, reusable = true)
    public Response getLatestMeasureBy(@PathParam("sensorId") String id) {
        GetMeasurementDataResult result = this.interactiveQueries.getLatestMeasureBy(id);

        if (result.getResult().isPresent()) {
            return Response.ok(result.getResult().get()).build();
        } else if (result.getHost().isPresent()) {
            URI otherUri = getOtherUri(result.getHost().get(), result.getPort().getAsInt(), id);
            return Response.seeOther(otherUri).build();
        } else {
            return Response.status(Status.NOT_FOUND.getStatusCode(),
                    "No data found for weather station " + id).build();
        }
    }

    @GET
    @Path("/all")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Counted(name = "performedChecks", description = "How many checks have been performed.", reusable = true)
    @Timed(name = "checksTimer", description = "A measure of how long it takes to perform the " +
            "operation.", unit = MetricUnits.MILLISECONDS, reusable = true)
    public Response getAllMeasures() {
        Response response;
        List<GetMeasurementDataResult> result = this.interactiveQueries.getAllMeasures();

        List<MeasureWithAggregatedData> bodyObjects = result.stream()
                .filter(item -> item.getResult().isPresent())
                .map(item -> item.getResult().get())
                .collect(Collectors.toList());

        response = Response.ok(bodyObjects).build();

        return response;
    }

    @GET
    @Path("/meta-data")
    @Produces(MediaType.APPLICATION_JSON)
    public List<PipelineMetadata> getMetaData() {
        return this.interactiveQueries.getMetaData();
    }

    private URI getOtherUri(String host, int port, String id) {
        try {
            return new URI("http://" + host + ":" + port + "/weather-stations/data/" + id);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }
}
