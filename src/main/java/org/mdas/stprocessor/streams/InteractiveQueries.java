package org.mdas.stprocessor.streams;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.mdas.stprocessor.model.Aggregation;
import org.mdas.stprocessor.model.MeasureWithAggregatedData;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.errors.InvalidStateStoreException;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;
import org.apache.kafka.streams.state.StreamsMetadata;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;

@ApplicationScoped
public class InteractiveQueries {

    private static final Logger LOG = Logger.getLogger(InteractiveQueries.class);

    @ConfigProperty(name = "hostname")
    String host;

    @Inject
    KafkaStreams streams;

    public List<PipelineMetadata> getMetaData() {
        return streams.allMetadataForStore(TopologyProducer.SENSOR_STORE).stream()
                .map(m -> new PipelineMetadata(m.hostInfo().host() + ":" + m.hostInfo().port(),
                        m.topicPartitions().stream().map(TopicPartition::toString)
                                .collect(Collectors.toSet())))
                .collect(Collectors.toList());
    }

    public GetMeasurementDataResult getLatestMeasureBy(String sensorId) {
        StreamsMetadata metadata = this.streams.metadataForKey(TopologyProducer.SENSOR_STORE,
                sensorId,
                Serdes.String().serializer());

        if (metadata == null || metadata == StreamsMetadata.NOT_AVAILABLE) {
            LOG.warnv("Found no metadata for key {0}", sensorId);
            return GetMeasurementDataResult.notFound();
        } else if (metadata.host().equals(host)) {
            LOG.infov("Found data for key {0} locally", sensorId);
            Aggregation result = this.getStateStore().get(sensorId);

            if (result != null) {
                return GetMeasurementDataResult.found(MeasureWithAggregatedData.from(result));
            } else {
                return GetMeasurementDataResult.notFound();
            }
        } else {
            LOG.infov("Found data for key {0} on remote host {1}:{2}", sensorId, metadata.host(),
                    metadata.port());
            return GetMeasurementDataResult.foundRemotely(metadata.host(), metadata.port());
        }
    }

    public List<GetMeasurementDataResult> getAllMeasures() {
        List<GetMeasurementDataResult> result = new ArrayList<>();
        this.getStateStore()
                .all()
                .forEachRemaining(item -> result.add(GetMeasurementDataResult
                        .found(MeasureWithAggregatedData.from(item.value))));

        return result;
    }

    private ReadOnlyKeyValueStore<String, Aggregation> getStateStore() {
        while (true) {
            try {
                return streams
                        .store(TopologyProducer.SENSOR_STORE, QueryableStoreTypes.keyValueStore());
            } catch (InvalidStateStoreException e) {
                LOG.infov("Ignored {0}, store is not ready yet", e.getLocalizedMessage());
            }
        }
    }
}
