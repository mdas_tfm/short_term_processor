package org.mdas.stprocessor.streams;

import java.util.Optional;
import java.util.OptionalInt;

import org.mdas.stprocessor.model.MeasureWithAggregatedData;

public class GetMeasurementDataResult {

    private static GetMeasurementDataResult NOT_FOUND =
            new GetMeasurementDataResult(null, null, null);

    private final MeasureWithAggregatedData result;
    private final String host;
    private final Integer port;

    private GetMeasurementDataResult(MeasureWithAggregatedData result, String host, Integer port) {
        this.result = result;
        this.host = host;
        this.port = port;
    }

    public static GetMeasurementDataResult found(MeasureWithAggregatedData data) {
        return new GetMeasurementDataResult(data, null, null);
    }

    public static GetMeasurementDataResult foundRemotely(String host, int port) {
        return new GetMeasurementDataResult(null, host, port);
    }

    public static GetMeasurementDataResult notFound() {
        return NOT_FOUND;
    }

    public Optional<MeasureWithAggregatedData> getResult() {
        return Optional.ofNullable(result);
    }

    public Optional<String> getHost() {
        return Optional.ofNullable(host);
    }

    public OptionalInt getPort() {
        return port != null ? OptionalInt.of(port) : OptionalInt.empty();
    }
}
