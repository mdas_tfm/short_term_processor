package org.mdas.stprocessor.streams;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.GlobalKTable;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.state.KeyValueBytesStoreSupplier;
import org.apache.kafka.streams.state.Stores;
import org.jboss.logging.Logger;
import org.mdas.stprocessor.controllers.MeasuresWebSocketEndpoint;
import org.mdas.stprocessor.model.Measure;
import org.mdas.stprocessor.model.Sensor;
import org.mdas.stprocessor.model.Aggregation;

import io.quarkus.kafka.client.serialization.JsonbSerde;

/**
 * TopologyProducer is an application scoped bean for handling the topology for
 * the streams of events, from input sources (topics).
 */
@ApplicationScoped
public class TopologyProducer {
        private static final Logger LOG = Logger.getLogger(TopologyProducer.class);

        public static final String SENSOR_STORE = "sensor-store";
        public static final String SENSOR_TOPIC = "sensor-topic";
        public static final String MEASURE_TOPIC = "measure-topic";
        public static final String ST_TOPIC = "short-term-streams";

        /**
         * buildTopology - builds the topology from the streams involved
         *
         * @return The defined Kafka streams topology.
         */
        @Produces
        public Topology buildTopology() {
                StreamsBuilder builder = new StreamsBuilder();
                JsonbSerde<Measure> measureSerde = new JsonbSerde<>(Measure.class);
                JsonbSerde<Sensor> sensorSerde = new JsonbSerde<>(Sensor.class);
                JsonbSerde<Aggregation> aggregationSerde = new JsonbSerde<>(Aggregation.class);
                KeyValueBytesStoreSupplier storeSupplier = Stores
                        .persistentKeyValueStore(SENSOR_STORE);

                GlobalKTable<String, Sensor> sensors = builder.globalTable(SENSOR_TOPIC,
                        Consumed.with(Serdes.String(), sensorSerde));

                KStream<String, Measure> measuresStream = builder.stream(MEASURE_TOPIC,
                        Consumed.with(Serdes.String(), measureSerde));

                /*
                 * Create an aggregation, joined by sensorId from the sensor and measure topics.
                 * Materializing into K -> V table, V being the aggregated values.
                 */
                measuresStream.join(sensors, (sensorId, measure) -> sensorId,
                        (measure, metadata) -> measure)
                        .groupByKey()
                        .aggregate(Aggregation::new,
                                (sensorId, value, aggregation) -> aggregation.updateFrom(value),
                                Materialized.<String, Aggregation>as(storeSupplier)
                                        .withKeySerde(Serdes.String())
                                        .withValueSerde(aggregationSerde))
                        .toStream().to(ST_TOPIC);

                Topology topology = builder.build();

                LOG.infov("Topology description {0}", topology.describe());
                return topology;
        }
}